#include <stdio.h>
#include "dice.h"

// Início da main

int main() {
    // Cabeçalho e atribuições

    int faces;

    printf("Choose the number of faces the dice will have");
    scanf("%d", &faces);

    // Emulando um dado

    initializeSeed();
    printf("Let's roll the dice: %d\n", rollDice(faces));

    // Fim do codigo

    return 0;
}